
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ClassActivity extends Application{

	public static void main(String[] args) {
		launch(args);
	}
	

	public void start(Stage primaryStage) throws Exception {
		// -----------------------------------------------
		// 1. Create & configure user interface controls
		// -----------------------------------------------
		
		// name label
		Label nameLabel = new Label("Enter your name"); // Label class  represents a label
		//nameLabel.textAlignmentProperty(); //can add properties to label like this 
		// name 
		TextField nameTextBox = new TextField();
		
		Label hoursLabel = new Label("Enter your hours worked");
		TextField hoursTextBox = new TextField();
		
		Label hourRateLabel = new Label("Enter your hour rate");
		TextField hourRateTextBox = new TextField();
		
		
		
		// button  getName(),SetName(),getAge()-->Accesor methods 
		Button goButton = new Button(); //ctl+click then will open the class properties 
		goButton.setText("Calculate"); //
		
		Label  outputLabel = new Label("");
		

		
		//Click Handler when user clicks the button 
		
		
		goButton.setOnAction(new EventHandler<ActionEvent>() {
		    @Override
		    public void handle(ActionEvent e) {
		        // Logic for what should happen when you push button
		    	String name=nameTextBox.getText();
		    	String hours=hoursTextBox.getText();
		    	String hourRate=hourRateTextBox.getText();
		    	
		    	//Covert the types  
		    	
		    	double hoursWorked=Double.parseDouble(hours);
		    	double hourlyRate=Double.parseDouble(hourRate);
		    	double wages=hoursWorked*hourlyRate;
		    	
		    	//Dispaly text in console
		    	
		    	 outputLabel.setText(name +"  earns $ "+wages);
		 
		    	
		    	//Clear the textbox
		    	
		    	nameTextBox.setText("");
		    	hoursTextBox.setText("");
		    	hourRateTextBox.setText("");
		    }

			private int toInt(String text) {
				// TODO Auto-generated method stub
				return 0;
			}
		});
		// -----------------------------------------------
		
		
		// 2. Make a layout manager can use HBox and VBox and StackPane(one above another),contorls the view of objects , can add multiple layout managers as well 
		//user interface controls(button,textbox etc) goes inside Layout go inside scene which goes inside stage
		
		VBox root= new VBox();
		root.setSpacing(30);
		
		// 3. Add controls to the layout manager
				// -----------------------------------------------
				
				// add controls in the same order you want them to appear
				root.getChildren().add(nameLabel);
				root.getChildren().add(nameTextBox);
				root.getChildren().add(hoursLabel);
				root.getChildren().add(hoursTextBox);
				root.getChildren().add(hourRateLabel);
				root.getChildren().add(hourRateTextBox);
				

				
				
				root.getChildren().add(goButton);
				root.getChildren().add(outputLabel);
				
				
		
				// -----------------------------------------------
				// 4. Add layout manager to scene
				// 5. Add scene to a stage
				// -----------------------------------------------

				// set the height & width of app to (300 x 250);
				primaryStage.setScene(new Scene(root, 500,500));
				
				// setting the title bar of the app
				primaryStage.setTitle("Example 01");
				
				// 6. Show the app
				primaryStage.show();
	}

}
